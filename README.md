# Numbers

## You can find apk file in [installationFiles](https://gitlab.com/kukharchuk.srg/numbers/-/tree/master/installationFiles) directory to test app on your  device

## Project Flow
- Kotlin
- MVVM
- Room database
- Network calls through Retrofit + OkHttp
- Coroutines, Flow
- Dependency injection by Dagger 2
- ViewBinding delegates
- Navigation through FragmentManager
- ...

## Screenshots

<table>
  <tr>
    <td><img src="screenshots/MainScreen.png" width=270 height=480></td>
    <td><img src="screenshots/DetailScreen.png" width=270 height=480></td>
  </tr>
 </table>

