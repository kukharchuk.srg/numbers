package ua.kuk.numbers.data.utils

import ua.kuk.numbers.data.database.NumberFactDbo
import ua.kuk.numbers.domain.enteties.NumberFact
import javax.inject.Inject

class Converter @Inject constructor() {

    fun convertApiResponseToNumberFactDbo(response: String): NumberFactDbo {
        val number = response.substringBefore(" ")
        val fact = response.substringAfter("$number ").substringBeforeLast(".")
        return NumberFactDbo(number = number, fact =  fact)
    }
}