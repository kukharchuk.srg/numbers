package ua.kuk.numbers.data.repositoriesimpl

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import ua.kuk.numbers.data.database.NumbersDao
import ua.kuk.numbers.data.utils.NumbersMapper
import ua.kuk.numbers.domain.enteties.NumberFact
import ua.kuk.numbers.domain.repositories.NumberFactsDbRepository
import javax.inject.Inject

class NumberFactsDbRepositoryImpl @Inject constructor(
    private val dao: NumbersDao,
    private val mapper: NumbersMapper
) : NumberFactsDbRepository {

    override fun getSearchHistory(): Flow<List<NumberFact>> {
        return combine(dao.getSearchHistory()) {
            it[0].map { item ->
                mapper.numberFactDboToEntity(item)
            }
        }
    }
}