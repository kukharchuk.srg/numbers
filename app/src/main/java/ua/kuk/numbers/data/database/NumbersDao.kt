package ua.kuk.numbers.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface NumbersDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNumberFact(item: NumberFactDbo)

    @Query("SELECT * FROM search_history ORDER BY id DESC")
    fun getSearchHistory(): Flow<List<NumberFactDbo>>

    @Query("Select * FROM search_history ORDER BY id DESC LIMIT 1")
    suspend fun getLastNumberFact(): NumberFactDbo
}