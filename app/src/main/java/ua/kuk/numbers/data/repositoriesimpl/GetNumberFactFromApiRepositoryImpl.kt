package ua.kuk.numbers.data.repositoriesimpl

import ua.kuk.numbers.data.database.NumbersDao
import ua.kuk.numbers.data.network.NumbersApiService
import ua.kuk.numbers.data.utils.Converter
import ua.kuk.numbers.data.utils.NumbersMapper
import ua.kuk.numbers.domain.repositories.ApiCallStatus
import ua.kuk.numbers.domain.repositories.Error
import ua.kuk.numbers.domain.repositories.GetNumberFactFromApiRepository
import ua.kuk.numbers.domain.repositories.Success
import javax.inject.Inject

class GetNumberFactFromApiRepositoryImpl @Inject constructor(
    private val apiService: NumbersApiService,
    private val dao: NumbersDao,
    private val converter: Converter,
    private val numberMapper: NumbersMapper
) : GetNumberFactFromApiRepository {

    override suspend fun getNumberFact(number: String): ApiCallStatus {
        return try {
            val apiResponse = apiService.getNumberFact(number)
            val item = converter.convertApiResponseToNumberFactDbo(apiResponse)
            dao.insertNumberFact(item)
            val mappedItem = numberMapper.numberFactDboToEntity(item)
            Success(mappedItem)
        } catch (e: Exception) {
            Error
        }
    }

    override suspend fun getRandomNumberFact(): ApiCallStatus {
        return try {
            val apiResponse = apiService.getRandomNumberFact()
            val item = converter.convertApiResponseToNumberFactDbo(apiResponse)
            dao.insertNumberFact(item)
            val mappedItem = numberMapper.numberFactDboToEntity(item)
            Success(mappedItem)
        } catch (e: Exception) {
            Error
        }
    }
}