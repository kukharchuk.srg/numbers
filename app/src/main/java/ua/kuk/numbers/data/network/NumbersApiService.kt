package ua.kuk.numbers.data.network


import retrofit2.http.GET
import retrofit2.http.Url


interface NumbersApiService {
    @GET
    suspend fun getNumberFact(
        @Url number: String
    ): String

    @GET("random")
    suspend fun getRandomNumberFact(): String
}