package ua.kuk.numbers.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(
    entities = [NumberFactDbo::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun dao(): NumbersDao

    companion object {
        private val LOCK = Any()
        private const val DB_NAME = "numbers_db"

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            synchronized(LOCK) {
                return INSTANCE ?: Room.databaseBuilder(
                    context,
                    AppDatabase::class.java,
                    DB_NAME
                ).build().also {
                    INSTANCE = it
                }
            }
        }
    }
}