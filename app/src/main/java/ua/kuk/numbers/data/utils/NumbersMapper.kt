package ua.kuk.numbers.data.utils

import ua.kuk.numbers.data.database.NumberFactDbo
import ua.kuk.numbers.domain.enteties.NumberFact
import javax.inject.Inject

class NumbersMapper @Inject constructor() {

    fun numberFactDboToEntity(numberFactDbo: NumberFactDbo): NumberFact {
        return NumberFact(numberFactDbo.number, numberFactDbo.fact)
    }
}