package ua.kuk.numbers.data.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "search_history")
data class NumberFactDbo(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val number: String,
    val fact: String
)
