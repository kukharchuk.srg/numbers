package ua.kuk.numbers.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import ua.kuk.numbers.NumbersApplication
import ua.kuk.numbers.presentation.fragments.get_info_fragment.GetInfoFragment

@ApplicationScope
@Component(modules = [DataModule::class, ViewModelModule::class])
interface ApplicationComponent {
    fun inject(application: NumbersApplication)
    fun inject(getInfoFragment: GetInfoFragment)

    @Component.Factory
    interface Factory {
        fun create(
            @BindsInstance application: Application
        ): ApplicationComponent
    }
}