package ua.kuk.numbers.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import ua.kuk.numbers.presentation.fragments.get_info_fragment.GetInfoViewModel

@Module
interface ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(GetInfoViewModel::class)
    fun bindGetInfoViewModelModel(viewModel: GetInfoViewModel): ViewModel
}