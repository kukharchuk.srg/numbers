package ua.kuk.numbers.di

import android.app.Application
import dagger.Binds
import dagger.Module
import dagger.Provides
import ua.kuk.numbers.data.database.AppDatabase
import ua.kuk.numbers.data.database.NumbersDao
import ua.kuk.numbers.data.network.NumbersApi
import ua.kuk.numbers.data.network.NumbersApiService
import ua.kuk.numbers.data.repositoriesimpl.GetNumberFactFromApiRepositoryImpl
import ua.kuk.numbers.data.repositoriesimpl.NumberFactsDbRepositoryImpl
import ua.kuk.numbers.domain.repositories.GetNumberFactFromApiRepository
import ua.kuk.numbers.domain.repositories.NumberFactsDbRepository

@Module
interface DataModule {
    @Binds
    @ApplicationScope
    abstract fun bindNumberFactsDbRepository(numberFactsDbRepositoryImpl: NumberFactsDbRepositoryImpl): NumberFactsDbRepository

    @Binds
    @ApplicationScope
    abstract fun bindGetNumberFactFromApiRepository(getNumberFactFromApiRepositoryImpl: GetNumberFactFromApiRepositoryImpl): GetNumberFactFromApiRepository

    companion object {
        @Provides
        @ApplicationScope
        fun provideCoinInfoDao(
            application: Application
        ): NumbersDao {
            return AppDatabase.getInstance(application).dao()
        }

        @Provides
        @ApplicationScope
        fun provideApiService(): NumbersApiService {
            return NumbersApi.numbersApiService
        }
    }
}