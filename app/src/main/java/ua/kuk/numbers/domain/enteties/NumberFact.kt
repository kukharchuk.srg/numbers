package ua.kuk.numbers.domain.enteties

data class NumberFact(
    val number: String,
    val fact: String
)