package ua.kuk.numbers.domain.repositories

import ua.kuk.numbers.domain.enteties.NumberFact


interface GetNumberFactFromApiRepository {
    suspend fun getNumberFact(number: String): ApiCallStatus
    suspend fun getRandomNumberFact(): ApiCallStatus
}

sealed interface ApiCallStatus
data object Error : ApiCallStatus
data class Success(val fact: NumberFact) : ApiCallStatus