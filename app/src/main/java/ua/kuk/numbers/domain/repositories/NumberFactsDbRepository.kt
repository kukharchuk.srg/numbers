package ua.kuk.numbers.domain.repositories

import kotlinx.coroutines.flow.Flow
import ua.kuk.numbers.domain.enteties.NumberFact

interface NumberFactsDbRepository {
    fun getSearchHistory(): Flow<List<NumberFact>>
}