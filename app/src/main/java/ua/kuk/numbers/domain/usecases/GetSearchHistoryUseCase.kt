package ua.kuk.numbers.domain.usecases

import ua.kuk.numbers.domain.repositories.NumberFactsDbRepository
import javax.inject.Inject

class GetSearchHistoryUseCase @Inject constructor(
    private val numberFactFromDbRepository: NumberFactsDbRepository
) {
    suspend operator fun invoke() = numberFactFromDbRepository.getSearchHistory()
}