package ua.kuk.numbers.domain.usecases

import androidx.annotation.WorkerThread
import ua.kuk.numbers.domain.repositories.GetNumberFactFromApiRepository
import javax.inject.Inject

class GetRandomNumberFactUseCase @Inject constructor(
    private val getRandomNumberFactRepository: GetNumberFactFromApiRepository
) {
    @WorkerThread
    suspend operator fun invoke() = getRandomNumberFactRepository.getRandomNumberFact()
}