package ua.kuk.numbers.domain.usecases

import androidx.annotation.WorkerThread
import ua.kuk.numbers.domain.repositories.GetNumberFactFromApiRepository
import javax.inject.Inject

class GetNumberFactUseCase @Inject constructor(
    private val getNumberFactRepository: GetNumberFactFromApiRepository
) {
    @WorkerThread
    suspend operator fun invoke(number: String) = getNumberFactRepository.getNumberFact(number)
}