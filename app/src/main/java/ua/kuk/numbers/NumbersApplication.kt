package ua.kuk.numbers

import android.app.Application
import ua.kuk.numbers.di.DaggerApplicationComponent

class NumbersApplication : Application() {
    val component by lazy {
        DaggerApplicationComponent.factory().create(this)
    }

    override fun onCreate() {
        component.inject(this)
        super.onCreate()
    }
}