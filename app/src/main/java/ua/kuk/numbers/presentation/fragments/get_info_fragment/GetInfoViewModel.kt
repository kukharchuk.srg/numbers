package ua.kuk.numbers.presentation.fragments.get_info_fragment

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import ua.kuk.numbers.domain.enteties.NumberFact
import ua.kuk.numbers.domain.usecases.GetNumberFactUseCase
import ua.kuk.numbers.domain.usecases.GetRandomNumberFactUseCase
import ua.kuk.numbers.domain.usecases.GetSearchHistoryUseCase
import javax.inject.Inject

class GetInfoViewModel @Inject constructor(
    private val getNumberFactUseCase: GetNumberFactUseCase,
    private val getRandomNumberFactUseCase: GetRandomNumberFactUseCase,
    private val getSearchHistoryUseCase: GetSearchHistoryUseCase,
) : ViewModel() {

    private val _state = MutableStateFlow<GetInfoScreenStates>(Initial)
    val state = _state.asStateFlow()

    suspend fun getNumberFact(number: String) {
        _state.value = Downloading
        viewModelScope.launch() {
            val apiCallResult = getNumberFactUseCase(number)
            when (apiCallResult) {
                is ua.kuk.numbers.domain.repositories.Success -> {
                    _state.value = Success(apiCallResult.fact)
                    _state.value = Initial
                }

                else -> {
                    _state.value = Error
                    _state.value = Initial
                }
            }
        }
    }

    suspend fun getRandomNumberFact() {
        _state.value = Downloading
        viewModelScope.launch {
            val apiCallResult = getRandomNumberFactUseCase()
            when (apiCallResult) {
                is ua.kuk.numbers.domain.repositories.Success -> {
                    _state.value = Success(apiCallResult.fact)
                    _state.value = Initial
                }

                else -> {
                    _state.value = Error
                    _state.value = Initial
                }
            }
        }
    }

    suspend fun getSearchHistory(): StateFlow<List<NumberFact>> {
        return getSearchHistoryUseCase().stateIn(
            viewModelScope
        )
    }
}

sealed interface GetInfoScreenStates

data object Initial : GetInfoScreenStates
data object Downloading : GetInfoScreenStates
data class Success(val fact: NumberFact) : GetInfoScreenStates
data object Error : GetInfoScreenStates

