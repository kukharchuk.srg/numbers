package ua.kuk.numbers.presentation.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import ua.kuk.numbers.R
import ua.kuk.numbers.databinding.FragmentNumberDetailsBinding

class NumberDetailsFragment : Fragment(R.layout.fragment_number_details) {
    private val binding: FragmentNumberDetailsBinding by viewBinding()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setFields()
    }

    private fun setFields() {
        val number = requireArguments().getString(EXTRA_NUMBER)
        val fact = requireArguments().getString(EXTRA_FACT)
        binding.tvNumberDetailsFragment.text = number
        binding.tvFactDetailsFragment.text = fact
    }

    companion object {
        private const val EXTRA_NUMBER = "number"
        private const val EXTRA_FACT = "fact"

        fun newInstance(number: String, fact: String): NumberDetailsFragment {
            return NumberDetailsFragment().apply {
                arguments = Bundle().apply {
                    putString(EXTRA_NUMBER, number)
                    putString(EXTRA_FACT, fact)
                }
            }
        }
    }
}