package ua.kuk.numbers.presentation.fragments.get_info_fragment

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.view.doOnNextLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import by.kirich1409.viewbindingdelegate.viewBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import ua.kuk.numbers.NumbersApplication
import ua.kuk.numbers.R
import ua.kuk.numbers.databinding.FragmentGetInfoBinding
import ua.kuk.numbers.presentation.fragments.NumberDetailsFragment
import ua.kuk.numbers.presentation.utils.ViewModelFactory
import javax.inject.Inject


class GetInfoFragment : Fragment(R.layout.fragment_get_info) {
    private val binding: FragmentGetInfoBinding by viewBinding()

    private val adapter by lazy {
        SearchHistoryAdapter()
    }

    private val component by lazy {
        (requireActivity().application as NumbersApplication).component
    }

    @Inject
    lateinit var factory: ViewModelFactory
    private val viewModel: GetInfoViewModel by viewModels {
        factory
    }

    override fun onAttach(context: Context) {
        component.inject(this)
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setRecyclerView()
        setScreenStates()
        setListeners()
    }

    private fun setRecyclerView() {
        binding.rvGetInfoFragment.adapter = adapter
        binding.rvGetInfoFragment.itemAnimator = null

        adapter.onNumberFactClick = { item ->
            requireActivity().supportFragmentManager.beginTransaction().replace(
                R.id.fragmentContainerViewMain,
                NumberDetailsFragment.newInstance(item.number, item.fact)
            ).addToBackStack(null).commit()
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.RESUMED) {
                viewModel.getSearchHistory().collectLatest { list ->
                    if (list.isNotEmpty()) {
                        adapter.submitList(list)
                        binding.rvGetInfoFragment.visibility = View.VISIBLE
                        binding.rvGetInfoFragment.doOnNextLayout {
                            binding.rvGetInfoFragment.scrollToPosition(0)
                        }
                        binding.tvNoDataGetInfoFragment.visibility = View.GONE
                    } else {
                        binding.rvGetInfoFragment.visibility = View.GONE
                        binding.tvNoDataGetInfoFragment.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun setScreenStates() {
        lifecycleScope.launch(Dispatchers.Main.immediate) {
            repeatOnLifecycle(Lifecycle.State.RESUMED) {
                viewModel.state.collectLatest {
                    when (it) {
                        is Initial -> {
                            binding.etNumberGetInfoFragment.isEnabled = true
                            binding.buttonFactGetInfoFragment.isEnabled = true
                            binding.buttonRandomGetInfoFragment.isEnabled = true
                            binding.progressBarGetInfoFragment.visibility = View.GONE
                        }

                        is Downloading -> {
                            binding.etNumberGetInfoFragment.isEnabled = false
                            binding.buttonFactGetInfoFragment.isEnabled = false
                            binding.buttonRandomGetInfoFragment.isEnabled = false
                            binding.progressBarGetInfoFragment.visibility = View.VISIBLE
                        }

                        is Success -> {
                            lifecycleScope.launch {
                                val item = it.fact
                                binding.etNumberGetInfoFragment.setText("")
                                requireActivity().supportFragmentManager.popBackStack()
                                requireActivity().supportFragmentManager
                                    .beginTransaction()
                                    .add(
                                        R.id.fragmentContainerViewMain,
                                        NumberDetailsFragment.newInstance(item.number, item.fact)
                                    ).addToBackStack(null).commit()
                            }
                        }

                        is Error -> {
                            binding.progressBarGetInfoFragment.visibility = View.GONE
                            Toast.makeText(
                                requireContext(),
                                getString(R.string.message_unknown_error),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            }

        }
    }

    private fun setListeners() {
        binding.buttonRandomGetInfoFragment.setOnClickListener {
            lifecycleScope.launch {
                viewModel.getRandomNumberFact()
            }
        }
        binding.buttonFactGetInfoFragment.setOnClickListener {
            val number = binding.etNumberGetInfoFragment.text.trim().toString()
            if (number.isNotBlank()) {
                hideKeyboard()
                lifecycleScope.launch {
                    viewModel.getNumberFact(number)
                }
            } else {
                Toast.makeText(
                    requireContext(),
                    getString(R.string.message_empty_number_field),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun hideKeyboard() {
        val imm: InputMethodManager =
            requireActivity().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(requireView().windowToken, 0)
    }
}