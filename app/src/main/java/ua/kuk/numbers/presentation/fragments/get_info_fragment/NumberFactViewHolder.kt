package ua.kuk.numbers.presentation.fragments.get_info_fragment

import androidx.recyclerview.widget.RecyclerView
import ua.kuk.numbers.databinding.NumberFactItemBinding
import ua.kuk.numbers.domain.enteties.NumberFact

class NumberFactViewHolder(val binding: NumberFactItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun setViewHolder(item: NumberFact, onItemClick: ((item: NumberFact) -> Unit)?) {
        binding.tvNumberItem.text = item.number
        binding.tvDescriptionItem.text = item.fact
        binding.root.setOnClickListener {
            onItemClick?.invoke(item)
        }
    }
}