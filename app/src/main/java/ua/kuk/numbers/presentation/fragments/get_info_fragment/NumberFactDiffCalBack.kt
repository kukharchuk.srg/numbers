package ua.kuk.numbers.presentation.fragments.get_info_fragment

import androidx.recyclerview.widget.DiffUtil
import ua.kuk.numbers.domain.enteties.NumberFact

object NumberFactDiffCalBack : DiffUtil.ItemCallback<NumberFact>() {
    override fun areItemsTheSame(oldItem: NumberFact, newItem: NumberFact): Boolean {
        return oldItem.number == newItem.number
    }

    override fun areContentsTheSame(oldItem: NumberFact, newItem: NumberFact): Boolean {
        return oldItem == newItem
    }
}