package ua.kuk.numbers.presentation.fragments.get_info_fragment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import ua.kuk.numbers.databinding.NumberFactItemBinding
import ua.kuk.numbers.domain.enteties.NumberFact

class SearchHistoryAdapter : ListAdapter<NumberFact, NumberFactViewHolder>(NumberFactDiffCalBack) {

    var onNumberFactClick: ((item: NumberFact) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NumberFactViewHolder {
        val binding = NumberFactItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return NumberFactViewHolder(binding)
    }

    override fun onBindViewHolder(holder: NumberFactViewHolder, position: Int) {
        val item = getItem(position)
        holder.setViewHolder(item,onNumberFactClick)
    }
}